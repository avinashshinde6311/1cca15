package Interface;

public interface CreditCard {
    void getType();
    void withdraw(double amt);
}
