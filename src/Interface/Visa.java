package Interface;

public class Visa implements CreditCard{
    @Override
    public void getType() {
        System.out.println("Credit Card Type Is VISA");
    }

    @Override
    public void withdraw(double amt) {
        System.out.println("Transaction Successfully is of Rs "+amt);
    }
}
