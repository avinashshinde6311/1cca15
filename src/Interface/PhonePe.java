package Interface;

public class PhonePe implements UPI, Wallet{
    @Override
    public void transferAmount(double amt) {
        System.out.println("Transfer Amount "+amt);
    }

    @Override
    public void makeBillAmount(double amt) {
        System.out.println("Make Bill Payment of Rs "+amt);
    }
}
