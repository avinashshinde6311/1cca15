package Interface;

public class MasterCard implements CreditCard{
    @Override
    public void getType() {
        System.out.println("Credit Card Type is MasterCard");
    }

    @Override
    public void withdraw(double amt) {
        System.out.println("Transaction successfully is of Dlr "+amt);
    }
}
