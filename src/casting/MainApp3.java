package casting;

public class MainApp3 {
    public static void main(String[] args) {
        bike b= new ElectricBike();
        b.getType();
        System.out.println("=================================");
        ElectricBike e = (ElectricBike)b;
        e.getType();
        e.BatteryInfo();
    }
}
