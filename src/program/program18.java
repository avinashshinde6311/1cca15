package program;

public class program18 {
    public static void main(String[] args) {
        int star = 1;
        int line = 5;
        int space = line - 1;
        int ch1=5;

        for (int a = 0; a < line; a++) {
            int ch2 = ch1;
            for (int b = 0; b < space; b++) {
                System.out.print("   ");

            }
            for (int c = 0; c < star; c++) {
                System.out.print(" " +ch2++ +" ");

            }
            System.out.println();
            ch1--;
            space--;
            star++;

        }
    }
}
