package program;

public class program19 {
    public static void main(String[] args) {
        int star = 1;
        int line = 9;
        int space = 4;

        for (int a = 0; a < line; a++) {
            char ch = 'A';
            for (int b = 0; b < space; b++) {
                System.out.print("   ");

            }
            for (int c = 0; c < star; c++) {
                System.out.print(" " +ch +" ");
                ch++;
            }
            System.out.println();
            if(a<=3){
                star++;
                space--;
            }
            else{
                star--;
                space++;
            }

        }
    }
}
