package polymorphism;

public class Manager extends Employee{

    @Override
    void getDesignation() {
        System.out.println("Designation Is Manager");
    }

    @Override
    void getSalary() {
        System.out.println("Salary Is 1500000");
    }
}
