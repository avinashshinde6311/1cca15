package polymorphism;

public abstract class Instagram {
    void makePost(){
        System.out.println("Post Photo or Video");
    }
    abstract void stories();
    abstract void reels();
}
