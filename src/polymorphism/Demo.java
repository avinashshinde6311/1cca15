package polymorphism;

public abstract class Demo {
    static int k=20;
    double d=35.25;

    abstract void test();
    void display(){
        System.out.println("Display Method");
    }
    static void info(){
        System.out.println("Info method");
    }

    Demo(){
        System.out.println("Constructor");
    }

    void Static(){
        System.out.println("Static Block");
    }
    {
        System.out.println("Non Static ");
    }
}
