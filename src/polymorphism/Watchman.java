package polymorphism;

public class Watchman extends Employee{
    @Override
    void getDesignation() {
        System.out.println("Designation is SR Watchman");
    }

    @Override
    void getSalary() {
        System.out.println("Salary Is 400000");
    }
}
